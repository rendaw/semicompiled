# Semicompiled

Bytecode/IR examples!  Read them at <https://rendaw.gitlab.io/semicompiled/>!

#### TODO

* Java - Boolean operators
* Java - More if conditions
* Java - Lambda
* LLVM - C IR
* LLVM - C++ IR
* Webassembly